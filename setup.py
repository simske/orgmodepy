#!/usr/bin/env python3
from setuptools import setup

setup(name='orgmodepy',
      description='Manipulate Orgmode files',
      author='Simske',
      author_email='mail@simske.com',
      packages=['orgmodepy'],
      version='0.2',
      entry_points={
            'console_scripts': [
                  'orgmode-sort = orgmodepy.utils:sort_done_script'
            ]
      }
      )
